"use strict";

const express = require('express');

const app = express();

const port = 3000;

app.use("/", (req, res, next) => {
    console.log('Dans le premier middleware!');
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.statusCode = 200;
    next(); // Autorise la requête à aller au prochain middleware
});

app.use((req, res, next) => {
    console.log('Dans un autre middleware!');
    res.send('<h1>Salut depuis Express!</h1>');
});

app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});
