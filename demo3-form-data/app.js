"use strict";

const express = require('express');
const app = express();
const port = 3000;


const debutHtml = `<html lang="fr">
<head>
<title>Blogue</title>
</head>
<nav style="background-color:black;">
  <h1 style="text-align:center; color:white">Mon super blogue</h1>
</nav>
<body>`;

const finHtml = `</body>
</html>`;

const formulaire = `<h2>Ajouter un article</h2>
<form action="/articles/new" method="POST">
	<label for="titre">Titre de l'article :</label>
	<input type="text" id="titre" name="titre" required>
	<button type="submit">Ajouter</button>
</form>
<br>
<a href="/articles">Retour à la liste des articles</a>`;

const message = `
<h1>L'article a bien été ajouté.</h1>
<a href="/articles">Retour à la liste des articles</a>`;


// Déclaration d'un parser pour analyser "le corps (body)" d'une requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

const articles = []; // Stockage temporaire en mémoire

// use est exécuté quelque soit la méthode (GET, POST, PUT...)
app.use("/", (req, res, next) => {
	console.log(req.url);
	console.log("\n\nPremier middleware!");
	res.setHeader("Content-Type", "text/html; charset=utf-8");
	res.write(debutHtml);
	next();
});

// get permet de répondre seulement aux requêtes en GET à l.url spécifiée
app.get("/articles", (req, res, next) => {
	console.log("Middleware de la liste des articles");
	res.write("<h2>Liste des articles</h2>");
	if (articles.length === 0) {
		res.write("<p>Aucun article disponible.</p>");
	  } else {
		res.write("<ul>");
		articles.forEach((article, index) => {
		  res.write(`<li>${article}</li>`);
		});
		res.write("</ul>");
	  }
	next();
});

app.get('/articles/new', (req, res, next) => {
  res.write(formulaire);
});

app.post('/articles/new', (req, res, next) => {
    // req.body permet de retrouver les données postées par le formulaire
    // Cela est possible grâce au middleware déclaré plus haut (express.urlencoded)
    console.log(req.body);
	articles.push(req.body.titre);
    res.write(message); // récupère le champ qui à le name="titre"
});

app.use("/", (req, res) => {
	console.log("Dernier middleware!");
	res.write(finHtml);
	res.end();
});


app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});
