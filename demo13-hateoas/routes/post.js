const express = require('express');
const postsController = require('../controllers/postsController');
const isAuth = require("../middleware/isAuth");

const router = express.Router();

// /posts => GET
router.get('/posts', postsController.getPosts);

// /post/postId => GET
router.get('/post/:postId', postsController.getPost);

// /post => POST
router.post('/post/', postsController.createPost);

// /post/postId => DELETE
router.delete('/post/:postId', postsController.deletePost);

// /post/postId => PUT
router.put('/post/:postId', postsController.updatePost);

module.exports = router;

