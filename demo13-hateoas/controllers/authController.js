const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const dotenv = require("dotenv");
dotenv.config();

const User = require("../models/user");

exports.createUser = async (req, res, next) => {
  const { name, email, password } = req.body;

  try {
    // Encryption du mot de passe
    const hashedPassword = await bcrypt.hash(password, 12);
		// Création d'un nouvel utilisateur
    const user = new User({
      email: email,
      name: name,
      password: hashedPassword,
    });

    await user.save();
    res.status(201).json({ message: "User created successfully!" });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.login = async (req, res, next) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error("Courriel ou mot de passe invalide");
      error.statusCode = 401;
      throw error;
    }

		// Vérifie si le mot de passe est valide
    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      const error = new Error("Courriel ou mot de passe invalide");
      error.statusCode = 401;
      throw error;
    }
		
		// Création d'un jeton JWT
    const token = jwt.sign(
      {
        email: user.email,
        name: user.name,
        id: user.id,
      },
      process.env.SECRET_JWT,
      { expiresIn: "1h" }
    );

    res.status(200).json({ token: token });
  } catch (err) {
    next(err);
  }
};
