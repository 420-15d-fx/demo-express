const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, "le titre est requis"]
    },
    content: {
      type: String,
      required: [true, "le contenu est requis"]
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Post', postSchema);
