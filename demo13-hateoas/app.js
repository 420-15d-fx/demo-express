"use strict";

const express = require('express');
const mongoose = require('mongoose');
const app = express();
const dotenv = require('dotenv');
dotenv.config();
const port = process.env.PORT || 3000;

const errorController = require('./controllers/errorController');
// Importe les routes
const postRoutes = require('./routes/post');
const authRoutes = require("./routes/auth");

// parse application/json
// Permet de parser le corps des requêtes (body) en JSON
app.use(express.json());  

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

// Utilisation des routes en tant que middleware
app.use(postRoutes);
app.use(authRoutes);

// Gestion erreur 404
app.use(errorController.get404);

// Gestion des erreurs
app.use(errorController.getErrors);

mongoose
  .connect(process.env.MONGODB)
  .then(() => {
    app.listen(port, () => {
      console.log(`Le serveur écoute sur http://localhost:${port}`);
    });
  })
  .catch(err => console.log(err));

