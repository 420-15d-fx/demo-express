"use strict";

const express = require('express');

const router = express.Router();

const gameController = require('../controllers/gameController');

// / => GET
router.get('/', gameController.gameInit);

// /attaque => GET
router.get('/attaque', gameController.attaque);

// /sante => GET
router.get('/sante', gameController.sante);


module.exports = router;

