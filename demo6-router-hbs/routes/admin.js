"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();

// N'ayant pas encore de base de données, on défini quelques titre d'articles
const articles =[{titre: 'Article 1'}, {titre: 'Article 2'}];

// /admin/articles/new => GET
router.get("/articles/new", (req, res) => {
    res.render("form", { title: "Ajouter un article" });
});

// /admin/articles/new => POST
router.post("/articles/new", (req, res) => {
  console.log(req.body);
  articles.push(req.body); //Ajout de l'article
  console.log(`Article ajouté : ${req.body.titre}`);
  res.redirect("/");
});

// Export des routes 
exports.routes = router;

// Export des articles 
exports.articles = articles;

