"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();

// Importe les articles
const { articles } = require('./admin');

// Route pour afficher la page d'accueil
router.get("/", (req, res) => {
    res.render("index", { title: "Accueil", articles });
  });

  // Export des routes 
exports.routes = router;

