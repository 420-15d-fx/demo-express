"use strict";

const express = require('express');

// Importe les articles
const { posts } = require('./admin');

const router = express.Router();

router.get('/', (req, res, next) => {
  res.render('index', {
    posts: posts,
    pageTitle: 'Accueil'
  });
});

// module.exports = router;
exports.routes = router;

