"use strict";

const path = require('path');
const express = require('express');
const router = express.Router(); //Définition d'un objet routeur
const app = express();
const port = 3000;


// Configuration pour template EJS
app.set('view engine', 'ejs');
// Déclarer le dossier views qui contient les templates
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));


// Importe les routes
const adminRoutes = require('./routes/admin');
const postRoutes = require('./routes/post');

// Déclaration d'un parser pour analyser "le corps (body)" d'une requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

// Utilisation des routes en tant que middleware
// route /admin
app.use('/admin', adminRoutes.routes);
// route /
app.use(postRoutes.routes);

app.use((req, res, next) => {
  res.status(404).render('404', { pageTitle: 'Page introuvable !' });
});

app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});

