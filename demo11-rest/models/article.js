const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const articleSchema = new Schema(
  {
    titre: {
      type: String,
      required: [true, "le titre est requis"]
    },
    contenu: {
      type: String,
      required: [true, "le contenu est requis"]
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Article', articleSchema);
