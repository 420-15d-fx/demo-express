"use strict";

const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 3000;

const errorsController = require('./controllers/errorsController');
// Importe les routes
const articleRoutes = require('./routes/article');

// Gestion des CORS 
app.use((req, res, next) => {
  // Définit les domaines qui sont autorisés à accéder aux ressources du serveur

  // Autorise l'accès à toutes les ressources depuis n'importe quel domaine
  res.setHeader('Access-Control-Allow-Origin', '*');
  // OU 
  // Autorise l'accès à toutes les ressources depuis le domaine https://cdpn.io
  // res.setHeader('Access-Control-Allow-Origin', 'https://cdpn.io');

  // Spécifie les méthodes HTTP qui sont autorisées pour accéder aux ressources
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );

  // Définit les en-têtes HTTP qui peuvent être utilisés lors de la demande
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  // Passe au middleware suivant
  next();
});


// parse application/json
// Permet de parser le corps des requêtes (body) en JSON
app.use(express.json());  

// Utilisation des routes en tant que middleware
app.use(articleRoutes);

// Gestion erreur 404, ce middleware doit être le dernier
app.use(errorsController.get404);

// Gestion des erreurs, ce middleware gère seulement les erreurs qui sont générées par le code
app.use(errorsController.getErrors);

// Démarrage du serveur
(async () => {
  try {
      await mongoose.connect('mongodb://127.0.0.1:27017/blogue');

      app.listen(port, () => {
          console.log(`Serveur à l'écoute sur : http://localhost:${port}`);
      });
  } catch (err) {
      console.error("Erreur de connexion à MongoDB :", err);
  }
})();

