// Récupère le modèle Article
const Article = require('../models/article');

const {formatErrorResponse, formatSuccessResponse} = require('../utils/formatErrorResponse'); // Import de la fonction utilitaire

/**
 * Récupère la liste de tous les articles et les renvoie en réponse JSON.
 * 
 * @param {import('express').Request} req - Objet de requête Express.
 * @param {import('express').Response} res - Objet de réponse Express utilisé pour envoyer la liste des articles en JSON.
 * @param {import('express').NextFunction} next - Fonction middleware pour gérer les erreurs.
 * 
 * @returns {void} Cette fonction ne retourne rien directement, elle envoie une réponse JSON ou passe une erreur à `next`.
 * 
 * @throws {Error} Passe une erreur à `next` en cas de problème lors de la récupération des articles.
 */

exports.getArticles = async (req, res, next) => {
  try {
    const articles = await Article.find();
    res.status(200).json(formatSuccessResponse(
      200,
      "Liste des articles récupérée avec succès.",
      articles,
      req.originalUrl
    ));
  } catch (err) {
    next(err);
  }
};


/**
 * Récupère un article en fonction de son identifiant et le renvoie en réponse JSON.
 * 
 * @param {import('express').Request} req - Objet de requête Express contenant l'ID de l'article dans `req.params.articleId`.
 * @param {import('express').Response} res - Objet de réponse Express utilisé pour envoyer l'article en JSON.
 * @param {import('express').NextFunction} next - Fonction middleware pour gérer les erreurs.
 * 
 * @returns {void} Cette fonction ne retourne rien directement, elle envoie une réponse JSON ou passe une erreur à `next`.
 * 
 * @throws {Error} Renvoie une erreur 404 si l'article n'existe pas.
 */
exports.getArticle = async (req, res, next) => {
  try {
    const articleId = req.params.articleId;
    const article = await Article.findById(articleId);

    if (!article) {
      return res.status(404).json(formatErrorResponse(
        404,
        "Not Found",
        "L'article demandé n'existe pas.",
        req.originalUrl
      ));
    }

    res.status(200).json(formatSuccessResponse(
      200,
      "Article récupéré avec succès.",
      article,
      req.originalUrl
    ));
  } catch (err) {
    next(err);
  }
};


/**
 * Crée un nouvel article et le sauvegarde en base de données.
 * Renvoie l'article créé en réponse JSON avec un statut 201 et un header `Location`.
 * 
 * @param {import('express').Request} req - Objet de requête Express contenant les données de l'article dans `req.body`.
 * @param {import('express').Response} res - Objet de réponse Express utilisé pour envoyer l'article créé.
 * @param {import('express').NextFunction} next - Fonction middleware pour gérer les erreurs.
 * 
 * @returns {void} Cette fonction ne retourne rien directement, elle envoie une réponse JSON ou passe une erreur à `next`.
 * 
 * @throws {Error} Passe une erreur à `next` en cas de problème lors de l'enregistrement de l'article.
 */

exports.createArticle = async (req, res, next) => {
  try {
   
    const { titre, contenu } = req.body;

    if (!titre || !contenu) {
      return res.status(400).json(formatErrorResponse(
        400,
        "Bad Request",
        "Le titre et le contenu sont requis.",
        req.originalUrl
      ));
    }

    const article = new Article({ titre, contenu });
    const result = await article.save();

    res.location(`/article/${result._id}`);
    res.status(201).json(formatSuccessResponse(
      201,
      "Article créé avec succès.",
      result,
      req.originalUrl
    ));
  } catch (err) {
    next(err);
  }
};

/**
 * Supprime un article en fonction de son identifiant.
 * Renvoie une réponse avec un statut 204 si la suppression est réussie.
 * 
 * @param {import('express').Request} req - Objet de requête Express contenant l'ID de l'article à supprimer dans `req.params.articleId`.
 * @param {import('express').Response} res - Objet de réponse Express utilisé pour confirmer la suppression.
 * @param {import('express').NextFunction} next - Fonction middleware pour gérer les erreurs.
 * 
 * @returns {void} Cette fonction ne retourne rien directement, elle envoie une réponse vide avec un statut 204 ou passe une erreur à `next`.
 * 
 * @throws {Error} Passe une erreur à `next` en cas de problème lors de la suppression de l'article.
 */

exports.deleteArticle = async (req, res, next) => {
  try {
    const articleId = req.params.articleId;
    const deletedArticle = await Article.findByIdAndRemove(articleId);

    res.status(204).send(); 
  } catch (err) {
    next(err);
  }
};

/**
 * Met à jour un article existant en fonction de son identifiant.
 * Renvoie l'article mis à jour en réponse JSON avec un statut 200.
 * 
 * @param {import('express').Request} req - Objet de requête Express contenant l'ID de l'article dans `req.params.articleId` et les nouvelles données dans `req.body`.
 * @param {import('express').Response} res - Objet de réponse Express utilisé pour envoyer l'article mis à jour.
 * @param {import('express').NextFunction} next - Fonction middleware pour gérer les erreurs.
 * 
 * @returns {void} Cette fonction ne retourne rien directement, elle envoie une réponse JSON ou passe une erreur à `next`.
 * 
 * @throws {Error} Renvoie une erreur 404 si l'article n'existe pas, ou passe toute autre erreur à `next` en cas de problème.
 */
exports.updateArticle = async (req, res, next) => {
  try {
    const articleId = req.params.articleId;
    const { titre, contenu } = req.body;

    if (!titre || !contenu) {
      return res.status(400).json(formatErrorResponse(
        400,
        "Bad Request",
        "Le titre et le contenu sont requis.",
        req.originalUrl
      ));
    }

    const article = await Article.findById(articleId);

    if (!article) {
      return res.status(404).json(formatErrorResponse(
        404,
        "Not Found",
        "L'article n'existe pas.",
        req.originalUrl
      ));
    }

    article.titre = titre;
    article.contenu = contenu;
    const updatedArticle = await article.save();

    res.status(200).json(formatSuccessResponse(
      200,
      "Article mis à jour avec succès.",
      updatedArticle,
      req.originalUrl
    ));
  } catch (err) {
    next(err);
  }
};

