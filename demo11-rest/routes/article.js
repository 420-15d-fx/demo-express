const express = require('express');
const articlesController = require('../controllers/articlesController');
const router = express.Router();

// /articles => GET
router.get('/articles', articlesController.getArticles);

// /articles/articleId => GET
router.get('/articles/:articleId', articlesController.getArticle);

// /articles => POST
router.post('/articles/', articlesController.createArticle);

// /articles/articleId => DELETE
router.delete('/articles/:articleId', articlesController.deleteArticle);

// /articles/articleId => PUT
router.put('/articles/:articleId', articlesController.updateArticle);

module.exports = router;

