"use strict";

const express = require('express');

const articlesController = require('../controllers/articlesController');

const router = express.Router();

router.get('/', articlesController.getArticles);


// /articles/new => GET
router.get('/articles/new', articlesController.getAddArticle);

// /articles/new => POST
router.post('/articles/new', articlesController.createArticle);

// /articles/add-article/:articleId => GET
router.get('/articles/:articleId', articlesController.getArticle);

// "/articles/:articleId/edit => GET
router.get("/articles/:articleId/edit", articlesController.getEditArticle);

// /articles/:articleId => POST
router.post("/articles/:articleId/edit", articlesController.updateArticle);

// /articles/:articleId/delete => POST
router.post("/articles/:articleId/delete", articlesController.deleteArticle);

module.exports = router;

