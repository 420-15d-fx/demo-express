"use strict";

const express = require('express');

const categoriesController = require('../controllers/categoriesController');

const router = express.Router();

// /categories/new => POST
router.get('/categories/new', categoriesController.getAddCategory);

// /categories/new => POST
router.post('/categories/new', categoriesController.createCategory);

module.exports = router;

