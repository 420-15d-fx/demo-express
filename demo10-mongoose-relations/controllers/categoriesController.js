"use strict";

// Récupère le modèle Category
const Category = require('../models/category');

// Affichage du formulaire d'ajout de catégorie
exports.getAddCategory = (req, res, next) => {
    res.render('categories/form', {
      title: 'Ajouter une catégorie',
      errors: []
    });
  };


// Enregistre une catégorie dans la bd
exports.createCategory = async (req, res, next) => {
  const { nom } = req.body;

  // Crée un nouvel article avec les informations du formulaire
  const categorie = new Category({
    nom: nom,
  });

  try {
    // Enregistre l'article dans la base de données
    await categorie.save();
    res.redirect('/');
  } catch (error) {
     // Gestion spécifique des erreurs de validation
    if (error.name === 'ValidationError') {
      const errors = Object.values(error.errors).map(err => err.message);
      console.log('Erreurs de validation :', errors);

      // Renvoyer les erreurs au formulaire
      const title = 'Ajouter une catégorie';
      return res.render('categories/form', { errors, nom, title });
    } else {
      // Gestion des autres types d'erreurs
      console.error('Erreur inattendue :', error);
      next(error);
    }
  }
};