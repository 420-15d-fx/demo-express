"use strict";

// Récupère le modèle Article
const Article = require('../models/article');
const Category = require('../models/category');

// Utilise la méthode find() afin de récupérer tous les articles
// Retourne un Promesse
exports.getArticles = async (req, res, next) => {
  try {
    const articles = await Article.find();

    res.render('articles/index', {articles: articles, title: 'Accueil'});
  } catch (err) {
    next(err);
  }
};

// Récupère un article grâce à son id
exports.getArticle = async (req, res, next) => {

  const articleId = req.params.articleId;

  try {

    const article = await Article.findById(articleId).populate("categories");
    console.log(article);

    if(!article){
      const error = new Error('Article non trouvé');
      error.statusCode = 404;
      throw error;
    }

    res.render('articles/details', {article: article,title: 'Détails - Article'});

  } catch (error) {
    next(error);
  }
};


// Affichage du formulaire d'ajout d'article
exports.getAddArticle = async (req, res, next) => {

  try {
    const categories = await Category.find();
    res.render('articles/form', {title: 'Ajouter un article', categories: categories, errors: []});
  } catch (err) {
    next(err);
  }

};

// Enregistre un article dans la bd
exports.createArticle = async (req, res, next) => {
  const { titre, contenu, categories } = req.body;


  // Crée un nouvel article avec les informations du formulaire
  const article = new Article({
    titre: titre,
    contenu: contenu,
    categories : categories
  });

  try {
    // Enregistre l'article dans la base de données
    await article.save();
    res.redirect('/');
  } catch (error) {
     // Gestion spécifique des erreurs de validation
    if (error.name === 'ValidationError') {
      const errors = Object.values(error.errors).map(err => err.message);
      console.log('Erreurs de validation :', errors);

      // Renvoyer les erreurs au formulaire
      const pageTitle = 'Ajouter un article';
      return res.render('articles/form', { errors, titre, contenu, title });
    } else {
      // Gestion des autres types d'erreurs
      console.error('Erreur inattendue :', error);
      next(error);
    }
  }
};

// Affichage du formulaire d'édition d'article
exports.getEditArticle = async (req, res, next) => {
  const articleId = req.params.articleId;
  try {
    const article = await Article.findById(articleId);
    
    console.log('article', article);

    
    if(!article){
      const error = new Error('Article non trouvé');
      error.statusCode = 404;
      throw error;
    }

    res.render('articles/form', {article: article, title: 'Modifier un article'});

  } catch (error) {
   
    next(error);
  }
};

// Enregistre un article modifié dans la bd
exports.updateArticle = async (req, res, next) => {
  const articleId = req.params.articleId;
  const titre = req.body.titre;
  const contenu = req.body.contenu;

  try {
    const article = await Article.findById(articleId);

    if (!article) {
      const error = new Error('Article non trouvé');
      error.statusCode = 404;
      throw error;
    } else {
      article.titre = titre;
      article.contenu = contenu;
      await article.save();
      res.redirect('/');
    }
  } catch (error) {
   
    next(error);
  }
};


// Suprime un article grâce à son id
exports.deleteArticle = async (req, res, next) => {
  const articleId = req.params.articleId;
  try {
    await Article.findByIdAndDelete(articleId);
    res.redirect('/');
  } catch (error) {
    
    next(error);
  }
};


