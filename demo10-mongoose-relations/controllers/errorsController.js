"use strict";

//Méthode du contrôleur pour la gestion des routes inexistantes (404)
exports.get404 = (req, res, next) => {
  res.status(404).render('404', { title : 'Page introuvable !' });
};

// Gestion des erreurs générales (500 et autres)
exports.getErrors = (error, req, res, next) => {

  console.error('Erreur capturée :', error.message);

  // Si c'est une erreur 404, on redirige vers `get404`
  if (error.statusCode === 404) {
    return exports.get404(req, res, next);
  }

  // Sinon, on affiche la page 500
  res.status(error.statusCode || 500).render('500', { title: 'Erreur serveur'});
};