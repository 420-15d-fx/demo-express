const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const articleSchema = new Schema(
  {
    titre: {
      type: String,
      required: [true, "Le titre est requis"]
    },
    contenu: {
      type: String,
      required: [true, "Le contenu est requis"]
    },
    categories: [
      {
        // Permet de référencer d'autres documents, provenant
        // d'autres collections
        type: Schema.Types.ObjectId,
        ref: 'Category'
      }
    ]
  },
  { timestamps: true }
);

module.exports = mongoose.model('Article', articleSchema);
