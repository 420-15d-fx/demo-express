const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    nom: {
      type: String,
      required: [true, "Le nom est requis"]
    }
  },
  { timestamps: true }
);


module.exports = mongoose.model('Category', categorySchema);