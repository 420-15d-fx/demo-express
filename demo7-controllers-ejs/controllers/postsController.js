"use strict";

const { posts } = require('./adminController');

exports.getPosts = (req, res, next) => {
  console.log(posts);
  res.render('index', {
    posts: posts,
    pageTitle: 'Accueil'
  });
};

