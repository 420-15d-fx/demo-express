"use strict";

const express = require('express');

const postsController = require('../controllers/postsController');

const router = express.Router();

router.get('/', postsController.getPosts);

module.exports = router;
// exports.routes = router;


