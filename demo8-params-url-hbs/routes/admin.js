"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();


//Importation du controller pour l'appel des méthodes
const adminController = require('../controllers/adminController.js');

// /admin/articles/new => GET
router.get('/articles/new', adminController.getCreateArticle);

// /admin/articles/new => POST
router.post('/articles/new', adminController.postCreateArticle);

// Export des routes 
exports.routes = router;



