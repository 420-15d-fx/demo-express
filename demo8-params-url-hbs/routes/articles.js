"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();

//Importation du controller pour l'appel des méthodes
const articleController = require('../controllers/articlesController.js');

// / => GET
router.get('/', articleController.getArticles);

// /articles/:articleId => GET
router.get('/articles/:id', articleController.getArticle);

// Export des routes 
exports.routes = router;

