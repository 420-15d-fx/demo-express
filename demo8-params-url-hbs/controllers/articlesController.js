// Importe les articles
const { articles } = require('./adminController');

//Méthode du contrôleur pour la vue index
exports.getArticles = (req, res, next) => {
    res.render("index", { title: "Accueil", articles });
  };

//Méthode du contrôleur pour la vue article
exports.getArticle = (req, res, next) => {
  const articleId = req.params.id;
  const article =  articles.find(article => article.id == articleId);

  res.render("details", {title: "Détails-articles", article: article });
};
