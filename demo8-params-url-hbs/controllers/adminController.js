

// N'ayant pas encore de base de données, on défini quelques titre d'articles
const articles =[{id: 1, titre: 'Article 1'}, {id: 2,titre: 'Article 2'}];

//Méthode du contrôleur pour la vue du formulaire de création d'un article
exports.getCreateArticle = (req, res, next) => {
    res.render("form", { title: "Ajouter un article" });
};


//Méthode du contrôleur pour l'ajout d'un article quand le formulaire est soumis
exports.postCreateArticle = (req, res, next) => {
  // ajout d'un id pour chaque article
  req.body.id = articles.length + 1;
  articles.push(req.body); //Ajout de l'article
  console.log(`Article ajouté : ${req.body.titre}`);
  res.redirect("/");
};

//Exportation des articles
exports.articles = articles;