"use strict";

const express = require("express");
const path = require("path");
const hbs = require("hbs"); // Importer `hbs`
const app = express();
const port = 3000;


// Importe les routes
const adminRoutes = require('./routes/admin');
const articlesRoutes = require('./routes/articles');

//Imporation du controller pour la gestion des erreurs 404
const errorController = require('./controllers/errorController');

// Définir `hbs` comme moteur de template
app.set("view engine", "hbs");

// Déclarer le dossier views qui contient les templates
app.set("views", path.join(__dirname, "views"));

// Enregistrer les partials
hbs.registerPartials(path.join(__dirname, "views", "partials"));


// Middleware pour l'affichage des fichiers statiques 
app.use(express.static(path.join(__dirname, 'public')));

// Servir Bootstrap depuis node_modules
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));

// Middleware pour parser les requêtes POST
app.use(express.urlencoded({ extended: false }));


// Utilisation des routes en tant que middleware
// route /admin
app.use('/admin', adminRoutes.routes);
// route /
app.use('/', articlesRoutes.routes);

//Gestion des erreurs 404
app.use(errorController.get404);

// Middleware pour gérer les erreurs 404
app.use((req, res) => {
    res.status(404).render("404", { title: "Page non trouvée" });
});

// Démarrage du serveur
app.listen(port, () => {
  console.log(`Serveur en écoute sur http://localhost:${port}`);
});
