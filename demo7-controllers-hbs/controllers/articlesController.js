// Importe les articles
const { articles } = require('./adminController');

//Méthode du contrôleur pour l'affiche de la vue index
exports.getArticles = (req, res, next) => {
    res.render("index", { title: "Accueil", articles });
  };
