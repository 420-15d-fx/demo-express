"use strict";

//Méthode du contrôleur pour l'affichage de la vue 404
exports.get404 = (req, res, next) => {
  res.status(404).render('404', { title : 'Page introuvable !' });
};
