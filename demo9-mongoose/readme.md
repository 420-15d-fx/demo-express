Utilisation d'un fichier d'environnement `.env`pour les variables de connexion à la base de données MongoDB.

Ce fichier devrait être dans le `.gitignore` pour ne pas être versionné dans une "vraie" application.