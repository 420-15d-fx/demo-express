"use strict";

// Récupère le modèle Article
const Article = require('../models/article');

// Utilise la méthode find() afin de récupérer tous les articles
// Retourne un Promesse
exports.getArticles = async (req, res, next) => {
  try {
    const articles = await Article.find();
    console.log('articles', articles);
    res.render('index', {
      articles: articles,
      pageTitle: 'Accueil'
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

// Récupère un article grâce à son id
exports.getArticle = async (req, res, next) => {
  console.log('req.params', req.params);
  console.log('req.query', req.query);
  const articleId = req.params.articleId;
  try {
    const article = await Article.findById(articleId);

    if(!article){
     /* let err = new Error();
      err.statusCode = 404;
      throw(err);*/
      next("asdf");
    }

    res.render('article', {
      article: article,
      pageTitle: 'Accueil'
    });
  } catch (error) {
    next(error);
  }
};


// Affichage du formulaire d'ajout d'article
exports.getAddArticle = (req, res, next) => {
  res.render('add_article', {
    pageTitle: 'Ajouter un article',
    errors: []
  });
};

// Enregistre un article dans la bd
exports.createArticle = async (req, res, next) => {
  const { titre, contenu } = req.body;

  // Crée un nouvel article avec les informations du formulaire
  const article = new Article({
    titre: titre,
    contenu: contenu
  });

  try {
    // Enregistre l'article dans la base de données
    await article.save();
    res.redirect('/');
  } catch (error) {
     // Gestion spécifique des erreurs de validation
    if (error.name === 'ValidationError') {
      const errors = Object.values(error.errors).map(err => err.message);
      console.log('Erreurs de validation :', errors);

      // Renvoyer les erreurs au formulaire
      const pageTitle = 'Ajouter un article';
      return res.render('add_article', { errors, titre, contenu, pageTitle });
    } else {
      // Gestion des autres types d'erreurs
      console.error('Erreur inattendue :', error);
      next(error);
    }
  }
};

// Affichage du formulaire d'édition d'article
exports.getEditArticle = async (req, res, next) => {
  const articleId = req.params.articleId;
  try {
    const article = await Article.findById(articleId);
    
    console.log('article', article);

    
    if(!article){
      let err = new Error();
      err.statusCode = 404;
      throw(err);
    }

    res.render('edit_article', {
      article: article,
      pageTitle: 'Modifier un article'
    });
  } catch (error) {
    console.log('error', error);
    next(error);
  }
};

// Enregistre un article modifié dans la bd
exports.updateArticle = async (req, res, next) => {
  const articleId = req.body.articleId;
  const titre = req.body.titre;
  const contenu = req.body.contenu;

  try {
    const article = await Article.findById(articleId);
    if (!article) {
      res.status(404).render('404', { pageTitle: 'Page introuvable !' });
    } else {
      article.titre = titre;
      article.contenu = contenu;
      await article.save();
      res.redirect('/');
    }
  } catch (error) {
    console.log('error', error);
    next(error);
  }
};


// Suprime un article grâce à son id
exports.deleteArticle = async (req, res, next) => {
  const articleId = req.params.articleId;
  try {
    await Article.findByIdAndRemove(articleId);
    console.log('article supprimé');
    res.redirect('/');
  } catch (error) {
    console.log('error', error);
    next(error);
  }
};


