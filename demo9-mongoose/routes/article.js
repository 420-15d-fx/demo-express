"use strict";

const express = require('express');

const articlesController = require('../controllers/articlesController');

const router = express.Router();

router.get('/', articlesController.getArticles);
router.get('/article/:articleId', articlesController.getArticle);

// /articles/add-article => GET
router.get('/add-article', articlesController.getAddArticle);

// /articles/add-article => POST
router.post('/add-article', articlesController.createArticle);

// /articles/:articleId/edit => GET
router.get("/articles/:articleId/edit", articlesController.getEditArticle);

// /articles/:articleId => POST
router.post("/articles/edit", articlesController.updateArticle);

// /articles/:postId/delete => POST
router.post("/articles/:articleId/delete", articlesController.deleteArticle);

module.exports = router;

