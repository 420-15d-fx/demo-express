"use strict";

const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const dotenv = require("dotenv");
dotenv.config();
const errorController = require('./controllers/error');

// Importe les routes
const articleRoutes = require('./routes/article');

// Configuration pour template EJS
app.set('view engine', 'ejs');
// Déclarer le dossier views qui contient les templates
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));


// Déclaration d'un parser pour analyser "le corps (body)" d'une 'requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

// Utilisation des routes en tant que middleware
app.use(articleRoutes);

app.use(errorController.get404);
app.use((error, req, res, next) => {
  console.log('error', error)
  res.status(500).render('500', {
    pageTitle: 'Erreur !',
    error: error
  });
});

mongoose
  .connect(process.env.MONGODB)
  .then(result => {
    app.listen(process.env.PORT, () => {
      console.log(`Le serveur écoute sur http://localhost:${process.env.PORT}`);
    });
  })
  .catch(err => console.log(err));

