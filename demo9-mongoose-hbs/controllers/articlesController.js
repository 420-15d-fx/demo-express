"use strict";

// Récupère le modèle Article
const Article = require('../models/article');

// Utilise la méthode find() afin de récupérer tous les articles
// Retourne un Promesse
exports.getArticles = async (req, res, next) => {
  try {
    const articles = await Article.find();

    console.log('articles', articles);
    res.render('index', { articles: articles, title: 'Accueil'});

  } catch (error) {
    next(error);
  }
};

// Récupère un article grâce à son id
exports.getArticle = async (req, res, next) => {
  console.log('req.params', req.params);
  console.log('req.query', req.query);

  const articleId = req.params.articleId;

  try {
    const article = await Article.findById(articleId);

    if(!article){
      const error = new Error('Article non trouvé');
      error.statusCode = 404;
      throw error;
    }

    res.render('details', {article: article, title: 'Détails - Article'});

  } catch (error) {
    next(error);
  }
};


// Affichage du formulaire d'ajout d'article
exports.getAddArticle = (req, res, next) => {
  res.render('form', {title: 'Ajouter un article', errors: []});
};

// Enregistre un article dans la bd
exports.createArticle = async (req, res, next) => {
  const { titre, contenu } = req.body;

  // Crée un nouvel article avec les informations du formulaire
  const article = new Article({
    titre: titre,
    contenu: contenu
  });

  try {

    // Enregistre l'article dans la base de données
    await article.save();

    res.redirect('/');

  } catch (error) {
    // Gestion spécifique des 
    // erreurs de validation
    if (error.name === 'ValidationError') {

      const errors = Object.values(error.errors).map(err => err.message);
      console.log('Erreurs de validation :', errors);

      // Renvoyer les erreurs au formulaire
      const title = 'Ajouter un article';
      return res.render('form', { errors, titre, contenu, title });

    } else {
      next(error);
    }
  }
};

// Affichage du formulaire d'édition d'article
exports.getEditArticle = async (req, res, next) => {
  const articleId = req.params.articleId;
  
  try {
    const article = await Article.findById(articleId);
    
    console.log('article', article);

    
    if(!article){
      const error = new Error('Article non trouvé');
      error.statusCode = 404;
      throw error;
    }

    res.render('form', {article: article, title: 'Modifier un article'});
  } catch (error) {
      next(error);
  }
};

// Enregistre un article modifié dans la bd
exports.updateArticle = async (req, res, next) => {
  const articleId = req.params.articleId;
  const titre = req.body.titre;
  const contenu = req.body.contenu;

  try {
    const article = await Article.findById(articleId);

    console.log('article', article)

    if (!article) {
      const error = new Error('Article non trouvé');
      error.statusCode = 404;
      throw error;
    } 

    article.titre = titre;
    article.contenu = contenu;

    await article.save();


    res.redirect('/');
    
  } catch (error) {
    next(error);
  }
};


// Suprime un article grâce à son id
exports.deleteArticle = async (req, res, next) => {

  const articleId = req.params.articleId;

  try {

    await Article.findByIdAndDelete(articleId);

    console.log('article supprimé');
    res.redirect('/');

  } catch (error) {
    next(error);
  }
};


