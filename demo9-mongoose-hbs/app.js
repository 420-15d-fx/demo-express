"use strict";

const express = require("express");
const mongoose = require('mongoose'); //Importaion de Mongoose
const path = require("path");
const hbs = require("hbs");
const app = express();

const port = 3000;


// Importe les routes
const articlesRoutes = require('./routes/articles');

//Imporation du controller pour la gestion des erreurs 404
const errorsController = require('./controllers/errorsController');

// Définir `hbs` comme moteur de template
app.set("view engine", "hbs");

// Déclarer le dossier views qui contient les templates
app.set("views", path.join(__dirname, "views"));

// Enregistrer les partials
hbs.registerPartials(path.join(__dirname, "views", "partials"));


// Middleware pour l'affichage des fichiers statiques 
app.use(express.static(path.join(__dirname, 'public')));

// Servir Bootstrap depuis node_modules
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));

// Middleware pour parser les requêtes POST
app.use(express.urlencoded({ extended: false }));


// route /
app.use('/', articlesRoutes);

//Test pour erreurs 500
/*app.get('/test-500', (req, res, next) => {
    throw new Error('Erreur 500 forcée !');
  });*/

// Gestion des routes non trouvées
app.use(errorsController.get404);

// Gestion des erreurs globales
app.use(errorsController.getErrors);


// Démarrage du serveur
(async () => {
  try {
      await mongoose.connect('mongodb://127.0.0.1:27017/blogue');

      app.listen(port, () => {
          console.log(`Serveur à l'écoute sur : http://localhost:${port}`);
      });
  } catch (err) {
      console.error("Erreur de connexion à MongoDB :", err);
  }
})();
