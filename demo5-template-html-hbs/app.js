"use strict";

const express = require("express");
const path = require("path");
const hbs = require("hbs"); // Importer `hbs`

const app = express();
const port = 3000;

const articles =[{titre: 'Article 1'}, {titre: 'Article 2'}];

// Définir `hbs` comme moteur de template
app.set("view engine", "hbs");

// Déclarer le dossier views qui contient les templates
app.set("views", path.join(__dirname, "views"));

// Enregistrer les partials
hbs.registerPartials(path.join(__dirname, "views", "partials"));


// Middleware pour l'affichage des fichiers statiques 
app.use(express.static(path.join(__dirname, 'public')));

// Servir Bootstrap depuis node_modules
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));

// Middleware pour parser les requêtes POST
app.use(express.urlencoded({ extended: false }));


// Route pour afficher le formulaire d'ajout d'article
app.get("/articles/new", (req, res) => {
    res.render("form", { title: "Ajouter un article" });
});

// Route pour ajouter un article
app.post("/articles/new", (req, res) => {
  console.log(req.body);
  articles.push(req.body); //Ajout de l'article
  console.log(`Article ajouté : ${req.body.titre}`);
  res.redirect("/");
});

// Route pour afficher la page d'accueil
app.get("/", (req, res) => {
    res.render("index", { title: "Accueil", articles });
  });

// Middleware pour gérer les erreurs 404
app.use((req, res) => {
    res.status(404).render("404", { title: "Page non trouvée" });
});

// Démarrage du serveur
app.listen(port, () => {
  console.log(`Serveur en écoute sur http://localhost:${port}`);
});
