"use strict";

const path = require('path');
const express = require('express');
const app = express();

// Configuration pour template EJS
app.set('view engine', 'ejs');
// Déclarer le dossier views qui contient les templates
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));

const errorController = require('./controllers/errorsController');

// Importe les routes
const adminRoutes = require('./routes/admin');
const postRoutes = require('./routes/post');

// Déclaration d'un parser pour analyser "le corps (body)" d'une 'requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

// Utilisation des routes en tant que middleware
app.use('/admin', adminRoutes);
app.use(postRoutes);

app.use(errorController.get404);

app.listen(3000);

