"use strict";

const posts = [{id: 1, title: 'Un article'}, {id: 2, title: 'Un 2ème article'}];


exports.getAddPost = (req, res, next) => {
  res.render('add-post', {
    pageTitle: 'Ajouter un article'
  });
};

exports.createPost = (req, res, next) => {
    console.log(req.body);
    // ajout d'un id pour chaque article
    req.body.id = posts.length + 1;
    // Ajout du titre dans posts
    posts.push(req.body);
    res.redirect('/');
}

exports.posts = posts;