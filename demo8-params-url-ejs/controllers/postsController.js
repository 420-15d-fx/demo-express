"use strict";

const { posts } = require('./adminController');

exports.getPosts = (req, res, next) => {
  console.log(posts);
  res.render('index', {
    posts: posts,
    pageTitle: 'Accueil'
  });
};

exports.getPost = (req, res, next) => {
  console.log('req.params', req.params);
  console.log('req.query', req.query);
  const postId = req.params.postId;
  res.render('post', {
    post: posts.find(post => post.id == postId),
    pageTitle: posts.find(post => post.id == postId).title
  });
};

