"use strict";

const express = require('express');

const postsController = require('../controllers/postsController');

const router = express.Router();

router.get('/', postsController.getPosts);
router.get('/post/:postId', postsController.getPost);

module.exports = router;
// exports.routes = router;

