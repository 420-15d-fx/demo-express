"use strict";

const express = require('express');

const router = express.Router();

const adminController = require('../controllers/adminController');

// /admin/add-post => GET
router.get('/add-post', adminController.getAddPost);

// /admin/add-post => POST
router.post('/add-post', adminController.createPost);

// Export des routes pour utilisation dans app.js
module.exports = router;

