"use strict";

const express = require("express");
const app = express();
const port = 3000;

const debutHtml = `<html lang="fr">
<head>
<title>Blogue</title>
</head>
<nav style="background-color:black;">
  <h1 style="text-align:center; color:white">Mon super blogue</h1>
</nav>
<body>`;

const finHtml = `</body>
</html>`;


//Middleware pour ignorer favicon.ico
//En l'absence d'un Content-Type spécifique, le navigateur tente d'interpréter le contenu en fonction de l'URL et de son extension (.ico → image).
//Sans se middleware, le premier middleware et le dernier middleware seront exécutés.
//app.use("/favicon.ico", (req, res) => res.status(204).end());

// use() est exécuté quelque soit la méthode (GET, POST, PUT...)
// et quelque soit l'url car nous avons spécifié "/" comme url
app.use("/", (req, res, next) => {
	console.log("\n\nPremier middleware!");
	res.setHeader("Content-Type", "text/html; charset=utf-8");
	res.write(debutHtml);
	next();
});


// get permet de répondre seulement aux requêtes en GET à l'url spécifiée
app.get("/articles", (req, res, next) => {
	console.log("Middleware de la liste des articles");
	res.write("<h2>Liste des articles</h2>");
	next();
});

// use() est exécuté quelque soit la méthode (GET, POST, PUT...)
// et quelque soit l'url car nous avons spécifié "/" comme url
app.use("/", (req, res) => {
	console.log("Dernier middleware!");
	res.write("footer");
	res.write(finHtml);
	res.end();
});


app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});
